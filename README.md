# First embedded server (using Berkeley Sockets, given example from STM32F7 package)

## What do you need?
- STM32F779I-EVAL board
- STM32CubeIDE
- An internet box or a computer with an ethernet port
- An ethernet cable (RJ45)

## How to make it work?
1. Add the project **LwIP_HTTP_Server_Socket_RTOS** in STM32CubeIDE
2. Build and flash the program into a STM32F779I-EVAL board
3. Make sure jumpers are in the good position (JP3, JP4, JP6, JP8 and JP12):<p align="center"><img src="docs/jumpers.jpg" alt="jumpers"></p>
4.  - If you want to connect it directly to a box, there is nothing to do
    - If you want to connect it to a computer, first follow these steps:
        + Windows 10:
            1. Create a static IP address (Control Panel -> Network and Internet -> Network and Sharing Center -> Change adapter settings -> Right-click and Properties on the network adapter -> Select Internet Protocol Version 4 (TCP/IPv4) -> Properties)<p align="center"><img src="docs/staticIPAddressW10.jpg" alt="staticIPAddressW10"></p>
            2. Download and launch: [TFTPD](http://tftpd32.jounin.net/tftpd32_download.html)
            3. Fill information<p align="center"><img src="docs/tftpdSettings.jpg" alt="tftpdSettings"></p>
            4. Connect the board into the computer
        + Ubuntu:
            1. Create a static IP address (Settings -> Network -> Select your interface)<img src="docs/staticIPAddressU.jpg" alt="staticIPAddressU"></p>
            2. Download isc-dhcp-server: ```sudo apt install isc-dhcp-server```
            3. Copy and past into */etc/dhcp/dhcpd.conf*:
                ```
                # Sample /etc/dhcpd.conf
                # (add your comments here) 
                default-lease-time 600;
                max-lease-time 7200;
                option subnet-mask 255.255.255.0;
                option routers 192.168.1.10;
                option domain-name-servers 8.8.8.8;
                
                subnet 192.168.10.0 netmask 255.255.255.0 {
                range 192.168.10.210 192.168.10.230;
                }
                ```
            4. Add your ethernet interface into */etc/default/isc-dhcp-server*<img src="docs/ethernetInterface.jpg" alt="ethernetInterface"></p>
            5. Execute ```sudo service isc-dhcp-server restart```
5. Connect to the IP address and it should works!
<p align="center"><img src="docs/screen.jpg" alt="screen"></p>
<p align="center"><img src="docs/webpage.jpg" alt="webpage"></p>

## How to change the webpage?
1. Put into your **fs/** directory your files: .html, .css, .js...
2. Use the script **makefsdata.pl** to convert your **fs/** files (command: ```perl makefsdata.pl```)<p align="center"><img src="docs/scriptPerl.jpg" alt="scriptPerl"></p>
3. Drag the **fsdata_custom.c** just created into **Src/** under the project
4. Change the function **http_server_serve** which is located in **Application/User/httpserver-socket.c** to match your new files
5. Build and flash the program into a STM32F779I-EVAL board
